import { Repository, DataSource } from 'typeorm';

interface Configs {
  dataFolder: string
  // useTasksDatabase?: boolean
}

interface Address {
  country?: string
  province?: string
  district?: string
  street?: string
  detail?: string
}

declare abstract class BaseEntity {
    id: number;
    createdAt: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}

declare enum Gender {
    Male = "male",
    Female = "female",
    Other = "other",
    Unknown = "unknown"
}
declare class AccountInfo extends BaseEntity {
    static Gender: typeof Gender;
    name?: string;
    fullName?: string;
    dob?: Date;
    avatar?: string;
    cover?: string;
    gender?: Gender;
    creationDate?: Date;
    friendsCount?: number;
    groupsCount?: number;
    uuid?: string;
    link?: string;
    email?: string;
    emailPassword?: string;
    emailRecover?: string;
    emailRecoverPassword?: string;
    _2FA?: string;
    token?: string;
    cookie?: string;
    address?: Address;
    isTokenLive?: boolean;
    isCookieLive?: boolean;
    isEmailLive?: boolean;
    uid?: string;
    zaloId?: string;
    zaloName?: string;
    displayName?: string;
    status?: string;
    socialAccount?: string;
    lastActionTime?: Date;
    lastUpdateTime?: Date;
    isOA?: boolean;
    isBusiness?: boolean;
    isValidate?: boolean;
    isEnable2FA?: boolean;
    quantity?: string;
    images?: string[];
    wechatId?: string;
    bio?: string;
    links?: string[];
    constructor(info?: Partial<AccountInfo>);
}

declare enum CategoryType {
    Facebook = "facebook",
    Zalo = "zalo",
    Wechat = "wechat",
    Unknown = "unknown"
}
declare class Category extends BaseEntity {
    static CategoryType: typeof CategoryType;
    type?: CategoryType;
    name?: string;
    order: number;
    childrens?: Category[];
    parent?: Category;
    accounts?: Account[];
    constructor(category?: Partial<Category>);
}

declare enum DeviceType {
    Android = "android",
    Browser = "browser"
}
declare class Device extends BaseEntity {
    static DeviceType: typeof DeviceType;
    name?: string;
    type: DeviceType;
    info: object;
    constructor(device?: Pick<Device, 'name'> & Partial<Omit<Device, 'name'>>);
}

declare enum GroupType {
    Unknown = "unknown",
    Facebook = "facebook",
    Zalo = "zalo",
    Wechat = "wechat"
}
declare class Group extends BaseEntity {
    type: GroupType;
    name?: string;
    avatar?: string;
    cover?: string;
    uuid?: string;
    groupId?: string;
    link?: string;
    isPublic: boolean;
    privacy: 'public' | 'private';
    isApprovalRequired: boolean;
    memberCount: number;
    likesCount: number;
    address?: Address;
    members?: Member[];
    constructor(group?: Partial<Group>);
}

declare class Member extends BaseEntity {
    account: Account;
    group: Group;
    isAdmin?: boolean;
    accepted?: boolean;
}

declare enum ProxyType {
    Custom = "custom",
    Tinsoft = "tinsoft",
    TMProxy = "tm-proxy",
    Dcom = "dcom",
    Device = "device",
    InternetModem = "internet-modem"
}
declare class Proxy extends BaseEntity {
    static ProxyType: typeof ProxyType;
    type: ProxyType;
    key?: string;
    protocol?: string;
    host?: string;
    port?: string;
    username?: string;
    password?: string;
    data: {};
    constructor(proxy?: Partial<Proxy>);
}

declare enum AccountType {
    Facebook = "facebook",
    Zalo = "zalo",
    Wechat = "wechat",
    Unknown = "unknown"
}
declare enum AccountStatus {
    Live = "live",
    Block = "block",
    Die = "die",
    Verify = "verify",
    KYC = "kyc",
    BlockMessage = "block-message",
    Unknown = "unknown"
}
declare class Account extends BaseEntity {
    static AccountStatus: typeof AccountStatus;
    static AccountType: typeof AccountType;
    username?: string;
    password?: string;
    email?: string;
    phone?: string;
    name?: string;
    status?: AccountStatus;
    type?: AccountType;
    favorite?: boolean;
    tags?: string[];
    isCustomer?: boolean;
    info?: AccountInfo;
    devices?: Device[];
    category?: Category;
    groups?: Member[];
    friends?: Account[];
    proxy?: Proxy;
    constructor(account?: Partial<Account>);
}

declare class ResourceCategory extends BaseEntity {
    name: string;
    order: number;
    children?: ResourceCategory[];
    parent?: ResourceCategory;
    constructor(name: string, order?: number, parent?: ResourceCategory);
}

declare enum ResourceType {
    Image = "image",
    Video = "video",
    Content = "content",
    Link = "link",
    Data = "data"
}
declare class Resource extends BaseEntity {
    type?: ResourceType;
    path?: string;
    data?: string;
    name?: string;
    hasBeenDeleted?: boolean | undefined;
    isModified?: boolean | undefined;
    category?: ResourceCategory;
    constructor(resource?: Partial<Resource>);
}

declare enum ActionSettingType {
    Facebook = "facebook",
    Zalo = "zalo",
    Wechat = "wechat",
    Unknown = "unknown"
}
declare class ActionSetting extends BaseEntity {
    static ActionSettingType: typeof ActionSettingType;
    type: ActionSettingType;
    name: string;
    setting?: object[];
    description?: string;
    isDefault: boolean;
    resources?: Resource[];
}

declare enum TaskStatus {
    Default = "default",
    Pending = "pending",
    Done = "done",
    Canceled = "canceled"
}
declare class Task extends BaseEntity {
    name?: string;
    status: TaskStatus;
    description?: string;
    startTime?: Date;
    endTime?: Date;
    actions?: Action[];
    constructor(task?: Partial<Task>);
}

declare enum ActionStatus {
    Default = "default",
    Pending = "pending",
    Done = "done",
    Canceled = "canceled"
}
declare class Action extends BaseEntity {
    setting?: object[];
    status: ActionStatus;
    result?: string;
    description?: string;
    account?: Account;
    toAccount?: Account;
    toGroup?: Group;
    resource?: Resource;
    actionSetting?: ActionSetting;
    task?: Task;
    environment?: ActionEnvironment;
    constructor(action?: Partial<Action>);
}

declare class ActionEnvironment extends BaseEntity {
    ip?: string;
    location?: string;
    deviceType?: string;
    deviceName?: string;
    proxy?: Proxy;
    action?: Action;
    constructor(env?: Partial<ActionEnvironment>);
}

declare class Message extends BaseEntity {
    content?: string;
    constructor(message?: Partial<Message>);
}

declare class SystemSetting extends BaseEntity {
    key?: string;
    value?: string;
    constructor(setting?: Partial<SystemSetting>);
}

type CustomCategoryRepository = Repository<Category> & {
    getCategoriesByType(type: CategoryType): Promise<Category[]>;
    getCategoriesTreeByType(type: CategoryType): Promise<Category[]>;
    getCategoryWithAccounts(id: number): Promise<Category[]>;
};

type CustomAccountRepository = Repository<Account> & {
    getAccontsByType(type: AccountType): Promise<Account[]>;
    getAccountsByTypeFull(type: AccountType): Promise<Account[]>;
    getAccountById(id: number): Promise<Account | null>;
    getAccountWithFullInfo(id: number): Promise<Account | null>;
};

type CustomAccountInfoRepository = Repository<AccountInfo> & object;

type CustomDeviceRepository = Repository<Device> & object;

type CustomProxyRepository = Repository<Proxy> & object;

type CustomActionSettingRepository = Repository<ActionSetting> & object;

declare class Database {
    #private;
    static MainDbName: string;
    static TasksDbName: string;
    static Account: typeof Account;
    static AccountInfo: typeof AccountInfo;
    static Category: typeof Category;
    static Device: typeof Device;
    static Proxy: typeof Proxy;
    static ActionSetting: typeof ActionSetting;
    configs: Configs;
    source: DataSource;
    categories: CustomCategoryRepository;
    accounts: CustomAccountRepository;
    accountInfos: CustomAccountInfoRepository;
    devices: CustomDeviceRepository;
    proxies: CustomProxyRepository;
    actionSettings: CustomActionSettingRepository;
    constructor(configs: Configs);
    initialize: () => Promise<void>;
    connect: () => Promise<DataSource>;
    disconnect: () => Promise<void>;
}

export { Account, AccountInfo, AccountStatus, AccountType, Action, ActionEnvironment, ActionSetting, ActionSettingType, ActionStatus, Category, CategoryType, Device, DeviceType, Gender, Group, GroupType, Member, Message, Proxy, ProxyType, Resource, ResourceCategory, SystemSetting, Task, TaskStatus, Database as default };
